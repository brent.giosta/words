import React, { Fragment } from "react";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Icon from "material-ui/Icon";
import IconButton from "material-ui/IconButton";
import Tabs, { Tab } from "material-ui/Tabs";
import { storeManager } from "../redux/store";

const Component = props => {
  return (
    <Fragment>
      <AppBar position="static">
        <Toolbar>
          {props.isSignedIn ? (
            <Switch>
              <Route
                path="/"
                exact={true}
                render={() => (
                  <IconButton style={{ marginLeft: -12, marginRight: 20 }} color="inherit" aria-label="Menu" onClick={props.onToggleDrawer}>
                    <Icon>menu</Icon>
                  </IconButton>
                )}
              />
              <Route
                render={() => (
                  <IconButton
                    style={{ marginLeft: -12, marginRight: 20 }}
                    color="inherit"
                    aria-label="Back"
                    onClick={() => {
                      props.history.goBack();
                    }}
                  >
                    <Icon>arrow_back</Icon>
                  </IconButton>
                )}
              />
            </Switch>
          ) : (
            <IconButton
              style={{ marginLeft: -12, marginRight: 20 }}
              color="inherit"
              aria-label="Menu"
              onClick={() => {
                window.close();
              }}
            >
              <Icon>close</Icon>
            </IconButton>
          )}
          <Typography
            variant="title"
            color="inherit"
            style={{
              flex: 1,
              WebkitFlex: 1,
              textOverflow: "ellipsis",
              overflow: "hidden",
              whiteSpace: "nowrap"
            }}
          >
            {props.title}
          </Typography>
          {props.actionIcon ? (
            <IconButton color="inherit" aria-label="Action" style={{ marginRight: -12 }} component={Link} to={props.actionIcon.link}>
              <Icon>{props.actionIcon.icon}</Icon>
            </IconButton>
          ) : null}
        </Toolbar>
        {props.tabArray ? (
          <Tabs value={props.tabIndex} onChange={(event, value) => storeManager.setTabIndex(value)} scrollable scrollButtons="auto">
            {props.tabArray.map(
              (tab, idx) =>
                tab.link ? <Tab key={idx} label={tab.title} component={Link} to={tab.link} /> : <Tab key={idx} label={tab.title} />
            )}
          </Tabs>
        ) : null}
      </AppBar>
    </Fragment>
  );
};

const mapStateToProps = function(store) {
  return {
    title: store.appState.title,
    tabArray: store.appState.tabArray,
    tabIndex: store.appState.tabIndex,
    actionIcon: store.appState.actionIcon
  };
};

export default withRouter(connect(mapStateToProps)(Component));
