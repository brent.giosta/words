import React, { PureComponent } from "react";
import WordCloud from "wordcloud";

export default class Component extends PureComponent {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  componentDidMount() {
    let componentRef = this.myRef.current;
    componentRef.width = componentRef.parentElement.clientWidth;
    componentRef.height = componentRef.parentElement.clientHeight;
    if (this.props.wordCloud) {
      setTimeout(
        () =>
          WordCloud(componentRef, {
            list: this.props.wordCloud,
            gridSize: Math.round(16 * componentRef.width / 1024),
            weightFactor: function(size) {
              return Math.pow(size, 3) * componentRef.width / 1024;
            },
            fontFamily: "Times, serif",
            color: function(word, weight) {
              return "#c09292";
            },
            rotateRatio: 0.5,
            rotationSteps: 2
          }),
        0
      );
    }
  }
  render() {
    let { wordCloud, ...otherProps } = this.props;

    return <canvas {...otherProps} ref={this.myRef} />;
  }
}
