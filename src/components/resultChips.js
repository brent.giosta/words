
import React  from "react";
import Chip from "material-ui/Chip";

import { Instant, LocalDateTime, DateTimeFormatter } from "js-joda";
const ResultChips = (props) => {
  return props.records.map((result, idx) => {
    let dateStamp = LocalDateTime.ofInstant(Instant.parse(result.timestamp)).format(DateTimeFormatter.ofPattern("M/d"));
    switch (result.result) {      
      case "mastered":
       return <Chip key={idx} style={{ backgroundColor: "#99FF99", marginLeft: 8 }} label={dateStamp} />
      case "correct":
        return <Chip key={idx} style={{ backgroundColor: "#99FFFF", marginLeft: 8 }} label={dateStamp} />;
      case "incorrect":
        return <Chip key={idx} style={{ backgroundColor: "#FF9999", marginLeft: 8 }} label={dateStamp} />;
      case "decoded":
        return <Chip key={idx} style={{ backgroundColor: "#FFDD99", marginLeft: 8 }} label={dateStamp} />;
      default:
        return null;
    }
  });
}

export default ResultChips;