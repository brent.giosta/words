import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import firebase from "firebase";

import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Icon from "material-ui/Icon";
import IconButton from "material-ui/IconButton";
import List, { ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";

const Component = props => {
  return (
    <Fragment>
      <AppBar position="fixed" style={{ zIndex: 1400 }}>
        <Toolbar>
          <IconButton style={{ marginLeft: -12, marginRight: 20 }} color="inherit" aria-label="Menu" onClick={props.onClose}>
            <Icon>navigate_before</Icon>
          </IconButton>
          <Typography variant="title" color="inherit" style={{ flex: 1, WebkitFlex: 1 }}>
            Sight Word Tracking
          </Typography>
        </Toolbar>
      </AppBar>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="title" color="inherit" style={{ flex: 1, WebkitFlex: 1 }}>
            &nbsp;
          </Typography>
        </Toolbar>
      </AppBar>
      <div style={{ width: 300, marginTop: 16, marginLeft: 8, marginRight: 8, marginBottom: 8 }}>
        <List>
          <ListItem button component={Link} to="/students" onClick={props.onClose}>
            Students
          </ListItem>
          <ListItem button component={Link} to="/lists" onClick={props.onClose}>
            Word Lists
          </ListItem>
          <Divider />
          <ListItem button component={Link} to="/fireworks" onClick={props.onClose}>
            *
          </ListItem>
          <ListItem
            button
            component={Link}
            to="/"
            onClick={() => {
              firebase.auth().signOut();
              props.onClose();
            }}
          >
            Log Out
          </ListItem>
          <ListItem
            button
            component={Link}
            to="/"
            onClick={event => {
              event.preventDefault();
              window.location.reload(true);
              return false;
            }}
          >
            Refresh
          </ListItem>
        </List>
      </div>
    </Fragment>
  );
};

export default Component;
