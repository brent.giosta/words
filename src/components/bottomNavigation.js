import React, { Fragment, PureComponent } from "react";
import { withRouter } from "react-router-dom";
import BottomNavigation, { BottomNavigationAction } from "material-ui/BottomNavigation";
import Icon from "material-ui/Icon";

class Component extends PureComponent {
  render() {
    return (
      <Fragment>
        <BottomNavigation
          value={(() => {
            switch (this.props.location.pathname) {
              case "/":
                return 0;
              case "/students":
                return 1;
              case "/class":
                return 2;
              default:
                return -1;
            }
          })()}
          onChange={(event, value) => {
            let location;
            switch (value) {
              case 0:
                location = "/";
                break;
              case 1:
                location = "/students";
                break;
              case 2:
                location = "/class";
                break;
              default:
                return;
            }
            this.props.history.push(location);
          }}
          showLabels
          style={{ width: "100%" }}
        >
          <BottomNavigationAction label="Home" icon={<Icon>home</Icon>} />
          <BottomNavigationAction label="Students" icon={<Icon>face</Icon>} />
          <BottomNavigationAction label="Class" icon={<Icon>people</Icon>} />
        </BottomNavigation>
      </Fragment>
    );
  }
}

export default withRouter(Component);
