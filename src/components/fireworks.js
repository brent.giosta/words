import React, { PureComponent } from "react";

// === CONFIGURATION ===

// Base firework acceleration.
// 1.0 causes fireworks to travel at a constant speed.
// Higher number increases rate firework accelerates over time.
const FIREWORK_ACCELERATION = 1.05;
// Minimum firework brightness.
const FIREWORK_BRIGHTNESS_MIN = 50;
// Maximum firework brightness.
const FIREWORK_BRIGHTNESS_MAX = 70;
// Base speed of fireworks.
const FIREWORK_SPEED = 5;
// Base length of firework trails.
const FIREWORK_TRAIL_LENGTH = 3;
// Determine if target position indicator is enabled.
const FIREWORK_TARGET_INDICATOR_AUTO_ENABLED = false;
const FIREWORK_TARGET_INDICATOR_MANUAL_ENABLED = true;

// Minimum particle brightness.
const PARTICLE_BRIGHTNESS_MIN = 50;
// Maximum particle brightness.
const PARTICLE_BRIGHTNESS_MAX = 80;
// Base particle count per firework.
const PARTICLE_COUNT = 100 * 2;
// Minimum particle decay rate.
const PARTICLE_DECAY_MIN = 0.015;
// Maximum particle decay rate.
const PARTICLE_DECAY_MAX = 0.03;
// Base particle friction.
// Slows the speed of particles over time.
const PARTICLE_FRICTION = 0.95;
// Base particle gravity.
// How quickly particles move toward a downward trajectory.
const PARTICLE_GRAVITY = 1.2;
// Variance in particle coloration.
const PARTICLE_HUE_VARIANCE = 20;
// Base particle transparency.
const PARTICLE_TRANSPARENCY = 1;
// Minimum particle speed.
const PARTICLE_SPEED_MIN = 1;
// Maximum particle speed.
const PARTICLE_SPEED_MAX = 10;
// Base length of explosion particle trails.
const PARTICLE_TRAIL_LENGTH = 15;

// Alpha level that canvas cleanup iteration removes existing trails.
// Lower value increases trail duration.
const CANVAS_CLEANUP_ALPHA = 0.1;
// Hue change per loop, used to rotate through different firework colors.
const HUE_STEP_INCREASE = 0.5;

// Minimum number of ticks per manual firework launch.
const TICKS_PER_FIREWORK_MIN = 5;
// Minimum number of ticks between each automatic firework launch.
const TICKS_PER_FIREWORK_AUTOMATED_MIN = 40;
// Maximum number of ticks between each automatic firework launch.
const TICKS_PER_FIREWORK_AUTOMATED_MAX = 120;

// === END CONFIGURATION ===

// === LOCAL VARS ===

// === END LOCAL VARS ===

// === HELPERS ===

// Use requestAnimationFrame to maintain smooth animation loops.
// Fall back on setTimeout() if browser support isn't available.
const requestAnimFrame = (() => {
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    }
  );
})();

// Get a random number within the specified range.
function random(min, max) {
  return Math.random() * (max - min) + min;
}

// Calculate the distance between two points.
function calculateDistance(aX, aY, bX, bY) {
  let xDistance = aX - bX;
  let yDistance = aY - bY;
  return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
}

// Creates a new firework.
// Path begins at 'start' point and ends and 'end' point.
function Firework(startX, startY, endX, endY, automatic) {
  this.automatic = automatic;
  // Set current coordinates.
  this.x = startX;
  this.y = startY;
  // Set starting coordinates.
  this.startX = startX;
  this.startY = startY;
  // Set end coordinates.
  this.endX = endX;
  this.endY = endY;
  // Get the distance to the end point.
  this.distanceToEnd = calculateDistance(startX, startY, endX, endY);
  this.distanceTraveled = 0;
  // Create an array to track current trail particles.
  this.trail = [];
  // Trail length determines how many trailing particles are active at once.
  this.trailLength = FIREWORK_TRAIL_LENGTH;
  // While the trail length remains, add current point to trail list.
  while (this.trailLength--) {
    this.trail.push([this.x, this.y]);
  }
  // Calculate the angle to travel from start to end point.
  this.angle = Math.atan2(endY - startY, endX - startX);
  // Set the speed.
  this.speed = FIREWORK_SPEED;
  // Set the acceleration.
  this.acceleration = FIREWORK_ACCELERATION;
  // Set the brightness.
  this.brightness = random(FIREWORK_BRIGHTNESS_MIN, FIREWORK_BRIGHTNESS_MAX);
  // Set the radius of click-target location.
  this.targetRadius = 2.5;
}

// Update a firework prototype.
// 'index' parameter is index in 'fireworks' array to remove, if journey is complete.
Firework.prototype.update = function(index, owner) {
  // Remove the oldest trail particle.
  this.trail.pop();
  // Add the current position to the start of trail.
  this.trail.unshift([this.x, this.y]);

  // Animate the target radius indicator.
  if (this.automatic ? FIREWORK_TARGET_INDICATOR_AUTO_ENABLED : FIREWORK_TARGET_INDICATOR_MANUAL_ENABLED) {
    if (this.targetRadius < 8) {
      this.targetRadius += 0.3;
    } else {
      this.targetRadius = 1;
    }
  }

  // Increase speed based on acceleration rate.
  this.speed *= this.acceleration;

  // Calculate current velocity for both x and y axes.
  let xVelocity = Math.cos(this.angle) * this.speed;
  let yVelocity = Math.sin(this.angle) * this.speed;
  // Calculate the current distance travelled based on starting position, current position, and velocity.
  // This can be used to determine if firework has reached final position.
  this.distanceTraveled = calculateDistance(this.startX, this.startY, this.x + xVelocity, this.y + yVelocity);

  // Check if final position has been reached (or exceeded).
  if (this.distanceTraveled >= this.distanceToEnd) {
    // Destroy firework by removing it from collection.
    owner.fireworks.splice(index, 1);
    // Create particle explosion at end point.  Important not to use this.x and this.y,
    // since that position is always one animation loop behind.
    owner.createParticles(this.endX, this.endY);
  } else {
    // End position hasn't been reached, so continue along current trajectory by updating current coordinates.
    this.x += xVelocity;
    this.y += yVelocity;
  }
};

// Draw a firework.
// Use CanvasRenderingContext2D methods to create strokes as firework paths.
Firework.prototype.draw = function(owner, canvas, context) {
  // Begin a new path for firework trail.
  context.beginPath();
  // Get the coordinates for the oldest trail position.
  let trailEndX = this.trail[this.trail.length - 1][0];
  let trailEndY = this.trail[this.trail.length - 1][1];
  // Create a trail stroke from trail end position to current firework position.
  context.moveTo(trailEndX, trailEndY);
  context.lineTo(this.x, this.y);
  // Set stroke coloration and style.
  // Use hue, saturation, and light values instead of RGB.
  context.strokeStyle = `hsl(${owner.hue}, 100%, ${this.brightness}%)`;
  // Draw stroke.
  context.stroke();

  if (this.automatic ? FIREWORK_TARGET_INDICATOR_AUTO_ENABLED : FIREWORK_TARGET_INDICATOR_MANUAL_ENABLED) {
    // Begin a new path for end position animation.
    context.beginPath();
    // Create an pulsing circle at the end point with targetRadius.
    context.arc(this.endX, this.endY, this.targetRadius, 0, Math.PI * 2);
    // Draw stroke.
    context.stroke();
  }
};

// Creates a new particle at provided 'x' and 'y' coordinates.
function Particle(x, y, currentHue) {
  // Set current position.
  this.x = x;
  this.y = y;
  // To better simulate a firework, set the angle of travel to random value in any direction.
  this.angle = random(0, Math.PI * 2);
  // Set friction.
  this.friction = PARTICLE_FRICTION;
  // Set gravity.
  this.gravity = PARTICLE_GRAVITY;
  // Set the hue to somewhat randomized number.
  // This gives the particles within a firework explosion an appealing variance.
  this.hue = random(currentHue - PARTICLE_HUE_VARIANCE, currentHue + PARTICLE_HUE_VARIANCE);
  // Set brightness.
  this.brightness = random(PARTICLE_BRIGHTNESS_MIN, PARTICLE_BRIGHTNESS_MAX);
  // Set decay.
  this.decay = random(PARTICLE_DECAY_MIN, PARTICLE_DECAY_MAX);
  // Set speed.
  this.speed = random(PARTICLE_SPEED_MIN, PARTICLE_SPEED_MAX);
  // Create an array to track current trail particles.
  this.trail = [];
  // Trail length determines how many trailing particles are active at once.
  this.trailLength = PARTICLE_TRAIL_LENGTH;
  // While the trail length remains, add current point to trail list.
  while (this.trailLength--) {
    this.trail.push([this.x, this.y]);
  }
  // Set transparency.
  this.transparency = PARTICLE_TRANSPARENCY;
}

// Update a particle prototype.
// 'index' parameter is index in 'particles' array to remove, if journey is complete.
Particle.prototype.update = function(index, owner) {
  // Remove the oldest trail particle.
  this.trail.pop();
  // Add the current position to the start of trail.
  this.trail.unshift([this.x, this.y]);

  // Decrease speed based on friction rate.
  this.speed *= this.friction;
  // Calculate current position based on angle, speed, and gravity (for y-axis only).
  this.x += Math.cos(this.angle) * this.speed;
  this.y += Math.sin(this.angle) * this.speed + this.gravity;

  // Apply transparency based on decay.
  this.transparency -= this.decay;
  // Use decay rate to determine if particle should be destroyed.
  if (this.transparency <= this.decay) {
    // Destroy particle once transparency level is below decay.
    owner.particles.splice(index, 1);
  }
};

// Draw a particle.
// Use CanvasRenderingContext2D methods to create strokes as particle paths.
Particle.prototype.draw = function(owner, canvas, context) {
  // Begin a new path for particle trail.
  context.beginPath();
  // Get the coordinates for the oldest trail position.
  let trailEndX = this.trail[this.trail.length - 1][0];
  let trailEndY = this.trail[this.trail.length - 1][1];
  // Create a trail stroke from trail end position to current particle position.
  context.moveTo(trailEndX, trailEndY);
  context.lineTo(this.x, this.y);
  // Set stroke coloration and style.
  // Use hue, brightness, and transparency instead of RGBA.
  context.strokeStyle = `hsla(${this.hue}, 100%, ${this.brightness}%, ${this.transparency})`;
  context.stroke();
};

// === END PROTOTYPING ===

// === APP HELPERS ===

// === END APP HELPERS ===

export default class Component extends PureComponent {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  getCanvas() {
    return this.myRef.current;
  }

  loop() {
    if (this.done) return;
    let canvas = this.getCanvas();
    if (!canvas) return;

    let context = canvas.getContext("2d");

    // Smoothly request animation frame for each loop iteration.
    requestAnimFrame(this.loop.bind(this));

    // Adjusts coloration of fireworks over time.
    this.hue += HUE_STEP_INCREASE;

    // Clean the canvas.
    this.cleanCanvas(canvas, context);

    // Update fireworks.
    this.updateFireworks(canvas, context);

    // Update particles.
    this.updateParticles(canvas, context);

    // Launch automated fireworks.
    this.launchAutomatedFirework(canvas);

    // Launch manual fireworks.
    this.launchManualFirework(canvas);
  }

  // Set the context, 2d in this case.
  // Firework and particles collections.
  fireworks = [];
  particles = [];
  // Mouse coordinates.
  mouseX = undefined;
  mouseY = undefined;
  // Variable to check if mouse is down.
  isMouseDown = false;
  // Initial hue.
  hue = 120;
  // Track number of ticks since automated firework.
  ticksSinceFireworkAutomated = 0;
  // Track number of ticks since manual firework.
  ticksSinceFirework = 0;
  done = false;

  cleanCanvas(canvas, context) {
    // Set 'destination-out' composite mode, so additional fill doesn't remove non-overlapping content.
    context.globalCompositeOperation = "destination-out";
    // Set alpha level of content to remove.
    // Lower value means trails remain on screen longer.
    context.fillStyle = `rgba(0, 0, 0, ${CANVAS_CLEANUP_ALPHA})`;
    // Fill entire canvas.
    context.fillRect(0, 0, canvas.width, canvas.height);
    // Reset composite mode to 'lighter', so overlapping particles brighten each other.
    context.globalCompositeOperation = "lighter";
  }

  // Create particle explosion at 'x' and 'y' coordinates.
  createParticles(x, y) {
    // Set particle count.
    // Higher numbers may reduce performance.
    let particleCount = PARTICLE_COUNT;
    while (particleCount--) {
      // Create a new particle and add it to particles collection.
      this.particles.push(new Particle(x, y, this.hue));
    }
  }

  // Launch fireworks automatically.
  launchAutomatedFirework(canvas) {
    // Determine if ticks since last automated launch is greater than random min/max values.
    if (this.ticksSinceFireworkAutomated >= random(TICKS_PER_FIREWORK_AUTOMATED_MIN, TICKS_PER_FIREWORK_AUTOMATED_MAX)) {
      // Check if mouse is not currently clicked.
      if (!this.isMouseDown) {
        // Set start position to bottom center.
        let startX = canvas.width / 2;
        let startY = canvas.height;
        // Set end position to random position, somewhere in the top half of screen.
        let endY = random(0, canvas.height / 2) + canvas.height / 6;
        let endX;
        if (endY > 3 * (canvas.height / 6)) {
          endX = random(0, canvas.width * 0.4) + canvas.width * 0.3;
        } else if (endY > 2 * (canvas.height / 6)) {
          endX = random(0, canvas.width * 0.6) + canvas.width * 0.2;
        } else {
          endX = random(0, canvas.width * 0.8) + canvas.width * 0.1;
        }
        // Create new firework and add to collection.
        this.fireworks.push(new Firework(startX, startY, endX, endY, true));
        // Reset tick counter.
        this.ticksSinceFireworkAutomated = 0;
      }
    } else {
      // Increment counter.
      this.ticksSinceFireworkAutomated++;
    }
  }

  // Launch fireworks manually, if mouse is pressed.
  launchManualFirework(canvas) {
    // Check if ticks since last firework launch is less than minimum value.
    if (this.ticksSinceFirework >= TICKS_PER_FIREWORK_MIN) {
      // Check if mouse is down.
      if (this.isMouseDown) {
        // Set start position to bottom center.
        let startX = canvas.width / 2;
        let startY = canvas.height;
        // Set end position to current mouse position.
        let endX = this.mouseX;
        let endY = this.mouseY;
        // Create new firework and add to collection.
        this.fireworks.push(new Firework(startX, startY, endX, endY, false));
        // Reset tick counter.
        this.ticksSinceFirework = 0;
      }
    } else {
      // Increment counter.
      this.ticksSinceFirework++;
    }
  }

  // Update all active fireworks.
  updateFireworks(canvas, context) {
    // Loop backwards through all fireworks, drawing and updating each.
    for (let i = this.fireworks.length - 1; i >= 0; --i) {
      this.fireworks[i].draw(this, canvas, context);
      this.fireworks[i].update(i, this);
    }
  }

  // Update all active particles.
  updateParticles(canvas, context) {
    // Loop backwards through all particles, drawing and updating each.
    for (let i = this.particles.length - 1; i >= 0; --i) {
      this.particles[i].draw(this, canvas, context);
      this.particles[i].update(i, this);
    }
  }

  componentDidMount() {
    let canvas = this.myRef.current;
    canvas.width = canvas.parentElement.clientWidth;
    canvas.height = canvas.parentElement.clientHeight;

    // Track current mouse position within canvas.
    canvas.addEventListener("mousemove", e => {
      let rect = canvas.getBoundingClientRect();
      this.mouseX = e.clientX - rect.left; //e.pageX - canvas.offsetLeft
      this.mouseY = e.clientY - rect.top; //e.pageY - canvas.offsetTop
    });

    // Track when mouse is pressed.
    canvas.addEventListener("mousedown", e => {
      e.preventDefault();
      this.isMouseDown = true;
    });

    // Track when mouse is released.
    canvas.addEventListener("mouseup", e => {
      e.preventDefault();
      this.isMouseDown = false;
    });

    // Track current mouse position within canvas.
    canvas.addEventListener("touchmove", e => {
      let rect = canvas.getBoundingClientRect();
      this.mouseX = e.touches[0].clientX - rect.left; //e.pageX - canvas.offsetLeft
      this.mouseY = e.touches[0].clientY - rect.top; //e.pageY - canvas.offsetTop
    });

    // Track when mouse is pressed.
    canvas.addEventListener("touchstart", e => {
      e.preventDefault();
      this.isMouseDown = true;
    });

    // Track when mouse is released.
    canvas.addEventListener("touchend", e => {
      e.preventDefault();
      this.isMouseDown = false;
    });

    this.done = false;
    this.loop();
  }
  componentWillUnmount() {
    this.done = true;
  }
  render() {
    let { ...otherProps } = this.props;

    return <canvas {...otherProps} ref={this.myRef} />;
  }
}
