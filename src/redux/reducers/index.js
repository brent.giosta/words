import { combineReducers } from "redux";

import appStateReducer from "./appStateReducer";

// Combine Reducers
var reducers = combineReducers({
  appState: appStateReducer
});

export default reducers;
