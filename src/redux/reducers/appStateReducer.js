const initialState = {
  title: "Sight Words",
  tabArray: null,
  tabIndex: 0,
  fullScreen: false
};

const appStateReducer = function(state = initialState, action) {
  switch (action.type) {
    case "FT_APPSTATE_TITLE":
      return Object.assign({}, state, {
        title: action.title ? action.title : initialState.title
      });
    case "FT_APPSTATE_TABS":
      return Object.assign({}, state, {
        tabArray: action.tabArray ? action.tabArray : initialState.tabArray,
        tabIndex: action.tabArray ? state.tabIndex : 0
      });
    case "FT_APPSTATE_TABINDEX":
      return Object.assign({}, state, {
        tabIndex: action.tabIndex !== null && action.tabIndex !== undefined ? action.tabIndex : initialState.tabIndex
      });
    case "FT_APPSTATE_FULLSCREEN":
      return Object.assign({}, state, {
        fullScreen: action.fullScreen !== null && action.fullScreen !== undefined ? action.fullScreen : initialState.fullScreen,
        scrollable: action.scrollable ? true : false
      });
    case "FT_APPSTATE_ACTIONBUTTON":
      return Object.assign({}, state, {
        actionIcon: action.iconName ? { icon: action.iconName, link: action.link } : null
      });
    default:
      return state;
  }
};

export default appStateReducer;
