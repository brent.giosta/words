import { createStore } from "redux";
import reducers from "./reducers";

const store = createStore(reducers);

class StoreManager {
  setTitle(newTitle) {
    store.dispatch({ type: "FT_APPSTATE_TITLE", title: newTitle });
  }

  setTabs(tabArray) {
    store.dispatch({ type: "FT_APPSTATE_TABS", tabArray: tabArray });
  }

  setTabIndex(tabIndex) {
    store.dispatch({ type: "FT_APPSTATE_TABINDEX", tabIndex: tabIndex });
  }

  setFullScreen(fullScreen, scrollable) {
    store.dispatch({ type: "FT_APPSTATE_FULLSCREEN", fullScreen: fullScreen, scrollable: scrollable });
  }

  setActionButton(iconName, link) {
    store.dispatch({ type: "FT_APPSTATE_ACTIONBUTTON", iconName: iconName, link: link });
  }
}

export const storeManager = new StoreManager();

export default store;
