import React, { Component, Fragment } from "react";
import { Route } from "react-router-dom";
import Students from "./pages/students";
import StudentEditor from "./pages/editStudent";
import Student from "./pages/student";
import Lists from "./pages/lists";
import ListEditor from "./pages/editList";
import Assessment from "./pages/assessment";
import AssessmentDetail from "./pages/assessmentDetail";
import WordCloud from "./pages/wordCloud";
import ProgressMonitoring from "./pages/progress";
import PrintList from "./pages/printList";
import StudentReport from "./pages/studentReport";
import ClassPage from "./pages/classPage";
import Fireworks from "./pages/fireworks";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Route path="/" exact={true} render={() => <WordCloud />} />
        <Route path="/students" exact={true} render={() => <Students />} />
        <Route path="/students_add" exact={true} render={() => <StudentEditor />} />
        <Route path="/students/:studentId" exact={true} render={() => <Student />} />
        <Route path="/students/:studentId/progress" exact={true} render={() => <ProgressMonitoring />} />
        <Route path="/students/:studentId/report" exact={true} render={() => <StudentReport />} />
        <Route path="/students_edit/:studentId" exact={true} render={() => <StudentEditor />} />
        <Route path="/lists" exact={true} render={() => <Lists />} />
        <Route path="/lists/:listId" exact={true} render={() => <ListEditor />} />
        <Route path="/assessments/:assessmentId" exact={true} render={() => <Assessment />} />
        <Route path="/assessmentDetail/:assessmentId" exact={true} render={() => <AssessmentDetail />} />
        <Route path="/wordCloud" exact={true} render={() => <WordCloud />} />
        <Route path="/printList" exact={true} render={() => <PrintList />} />
        <Route path="/class" exact={true} render={() => <ClassPage />} />
        <Route path="/fireworks" exact={true} render={() => <Fireworks />} />
      </Fragment>
    );
  }
}

export default App;
