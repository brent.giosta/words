import AssessmentService from './core/assessment';
import WordListService from './core/wordList';
import WordStatusService from './wordStatus';

import { Instant } from "js-joda";

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export async function createBaselineAssessment(studentId, student) {
  let query = await WordListService.getAllAscending();
  let wordLists = query.map(result => result.data.wordList);

  let resultList = [];

  let startGradeLevel = student.beginGradeLevel || 0;
  let endGradeLevel = student.grade || 99999;

  wordLists.forEach((list, idx) => {
    if (idx >= startGradeLevel && idx <= endGradeLevel) {
      let tempList = [...list];
      resultList = resultList.concat(shuffleArray(tempList));
    }
  });

  let assessment = {
    studentId: studentId,
    wordList: resultList.map(word => ({ word: word, beginTime: null, endTime: null, result: null })),
    beginTimestamp: Instant.now().toString(),
    currentIndex: 0,
    type: "baseline"
  };

  let assessmentId = await AssessmentService.post(assessment);

  return assessmentId;
}

export async function createProgressAssessment(studentId, student, wordCount) {
  let resultList = [...(student.practiceList || [])];
  let wordsNeeded = parseInt(wordCount, 10) - resultList.length;
  let startGradeLevel = student.beginGradeLevel || 0;
  let endGradeLevel = student.grade || 12;
  let wordList = await WordStatusService.getWordStatusByStudent(studentId);

  let wordMap = new Map(wordList);
  resultList.forEach(word => {
    wordMap.delete(word);
  });

  const addWordFromMap = wordItem => {
    wordMap.delete(wordItem.word);
    resultList.push(wordItem.word);
    --wordsNeeded;
  };

  for (let i = startGradeLevel; i <= endGradeLevel; ++i) {
    if (wordsNeeded === 0) break;
    let toAdd = shuffleArray(
      Array.from(wordMap.values()).filter(wordItem => wordItem.level === i && wordItem.lastResult === "incorrect")
    ).splice(0, wordsNeeded);
    toAdd.forEach(addWordFromMap);
  }

  for (let i = startGradeLevel; i <= endGradeLevel; ++i) {
    if (wordsNeeded === 0) break;
    let toAdd = shuffleArray(
      Array.from(wordMap.values()).filter(wordItem => wordItem.level === i && wordItem.lastResult === "decoded")
    ).splice(0, wordsNeeded);
    toAdd.forEach(addWordFromMap);
  }

  for (let i = startGradeLevel; i <= endGradeLevel; ++i) {
    if (wordsNeeded === 0) break;
    let toAdd = shuffleArray(
      Array.from(wordMap.values()).filter(wordItem => wordItem.level === i && wordItem.lastResult === "correct") // not mastered
    ).splice(0, wordsNeeded);
    toAdd.forEach(addWordFromMap);
  }

  for (let i = startGradeLevel; i <= endGradeLevel; ++i) {
    if (wordsNeeded === 0) break;
    let toAdd = shuffleArray(Array.from(wordMap.values()))
      .filter(wordItem => wordItem.level === i)
      .splice(0, wordsNeeded);
    toAdd.forEach(addWordFromMap);
  }

  let assessment = {
    studentId: studentId,
    wordList: shuffleArray(resultList).map(word => ({ word: word, beginTime: null, endTime: null, result: null })),
    beginTimestamp: Instant.now().toString(),
    currentIndex: 0,
    type: "monitoring"
  };

  let assessmentId = await AssessmentService.post(assessment);

  return assessmentId;
}