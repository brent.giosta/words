import WordListService from "./core/wordList";
import StudentService from "./core/student";

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export default async function assignNewPracticeList(studentId, wordList2) {
  let query = await WordListService.getAllAscending();
  let student = await StudentService.get(studentId);

  let oldPracticeList = [...(student.practiceList || [])];
  let newPracticeList = [];

  // Rule 1: keep any word from the old practice list if the most result result is 'incorrect'
  oldPracticeList.forEach(word => {
    let wordResult = wordList2.get(word);
    if (wordResult && wordResult.lastResult === "incorrect") {
      newPracticeList.push(word);
    }
  });

  let wordsNeeded = 12 - newPracticeList.length;

  let wordLists = query.map(result => result.data.wordList);
  let startGradeLevel = student.beginGradeLevel || 0;
  let endGradeLevel = student.grade || 99999;

  // Rule 2: Add any incorrect words from each grade level
  if (wordsNeeded > 0)
    wordLists.forEach((wordList, idx) => {
      if (idx >= startGradeLevel && idx <= endGradeLevel) {
        let redWords = shuffleArray(
          wordList.filter(word => newPracticeList.indexOf(word) < 0 && (wordList2.get(word) || {}).lastResult === "incorrect")
        ).slice(0, wordsNeeded);
        newPracticeList = newPracticeList.concat(redWords);
        wordsNeeded = 12 - newPracticeList.length;
      }
    });

  // Rule 3: Add any decoded words from each grade level
  if (wordsNeeded > 0)
    wordLists.forEach((wordList, idx) => {
      if (idx >= startGradeLevel && idx <= endGradeLevel) {
        let yellowWords = shuffleArray(
          wordList.filter(word => newPracticeList.indexOf(word) < 0 && (wordList2.get(word) || {}).lastResult === "decoded")
        ).slice(0, wordsNeeded);
        newPracticeList = newPracticeList.concat(yellowWords);
        wordsNeeded = 12 - newPracticeList.length;
      }
    });

  // Rule 4: Add any other words which are not either always correct or have been correct at least 3 times in a row
  if (wordsNeeded > 0)
    wordLists.forEach((wordList, idx) => {
      if (idx >= startGradeLevel && idx <= endGradeLevel) {
        let greenishWords = shuffleArray(
          wordList.filter(word => {
            let result = wordList2.get(word) || {};
            if (result.lastResult === "mastered") return false;
            return newPracticeList.indexOf(word) < 0 && result.lastResult === "correct";
          })
        ).slice(0, wordsNeeded);
        newPracticeList = newPracticeList.concat(greenishWords);
        wordsNeeded = 12 - newPracticeList.length;
      }
    });

  let newStudent = { practiceList: newPracticeList };

  await StudentService.patch(studentId, newStudent);

  return newPracticeList;
}
