import { DataService } from "./base";

const StudentService = new DataService("students");

export default StudentService;
