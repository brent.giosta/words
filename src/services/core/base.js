import firebase from "firebase";

export class DataService {
  constructor(objectName) {
    this.objectName = objectName;
    this.cache = {};
    this.fullCache = false;
  }

  notifyCache(key, object, deleted) {

  }

  /** Gets all of the documents into a structure of:
   {id: id, data: data} */
  async getAll() {
    if (this.fullCache) {
      return Object.keys(this.cache).map(key=>({id: key, data: this.cache[key]}));
    }
    const result = await this.mapResults(this.collection());
    this.cache = {};
    result.forEach(item=>{
      this.cache[item.id] = item.data;
    });
    this.fullCache = true;
    return result;
  }

  collection() {
    return firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection(this.objectName);
  }

  async mapResults(collection) {
    return (await collection.get()).docs.map(doc => ({ id: doc.id, data: doc.data() }));
  }

  /** Gets a single document */
  async get(id) {
    if (this.cache && this.cache[id]) {
      return this.cache[id];
    }
    let result = (await this.collection()
      .doc(id)
      .get()).data();
    this.cache[id] = result;
    return result;
  }

  /** Creates a single document, returns the id */
  async post(dataObject) {
    let resultId = (await this.collection().add(dataObject)).id;
    this.cache[resultId] = dataObject;
    this.notifyCache(resultId, dataObject);
    return resultId;
  }

  /** Overwrites an entire document, no return */
  async put(id, dataObject) {
    await this.collection()
      .doc(id)
      .set(dataObject);
    this.cache[id] = dataObject;
    this.notifyCache(id, dataObject);
  }

  /** Updates a document with the given fields, returns the combined data */
  async patch(id, dataObject) {
    await this.collection()
      .doc(id)
      .update(dataObject);
    delete this.cache[id];
    let result = await this.get(id);
    this.notifyCache(id, dataObject);
    return result;    
  }

  /** Deletes the document, no return */
  async delete(id) {
    await this.collection()
      .doc(id)
      .delete();
    delete this.cache[id];
    this.notifyCache(id, null, true);
  }
}
