import { DataService } from "./base";

class WordListServiceImpl extends DataService {
  constructor() {
    super("wordLists");
    this.ascendingCache = null;
  }

  notifyCache(key, object, deleted) {
    super.notifyCache(key, object, deleted);
    this.ascendingCache = null;
  }

  async getAllAscending() {
    if (!this.ascendingCache) {
      this.ascendingCache = this.mapResults(this.collection().orderBy("timestamp", "asc"));
    } 
    return this.ascendingCache;
  }
}

const WordListService = new WordListServiceImpl();

export default WordListService;
