import { DataService } from "./base";

class AssessmentServiceImpl extends DataService {
  constructor() {
    super("assessments");
    this.cacheByStudent = new Map();
    this.descendingCache = null;
  }

  notifyCache(key, object, deleted) {
    super.notifyCache(key, object, deleted);
    if (deleted || !object.studentId) {
      this.cacheByStudent = new Map();
    } else {
      this.cacheByStudent.delete(object.studentId);
    }
    this.descendingCache = null;
  }

  async getAllDescendingByStudent(studentId) {
    let cacheResult = this.cacheByStudent.get(studentId);
    if (!cacheResult) {
      cacheResult = this.mapResults(
        this.collection()
          .where("studentId", "==", studentId)
          .orderBy("beginTimestamp", "desc")
      );
      this.cacheByStudent.set(studentId, cacheResult);
    }
    return cacheResult;
  }

  async getAllDescending() {
    if (!this.descendingCache) {
      this.descendingCache = this.mapResults(this.collection().orderBy("beginTimestamp", "desc"));
    }
    return this.descendingCache;
  }
}

const AssessmentService = new AssessmentServiceImpl();

export default AssessmentService;
