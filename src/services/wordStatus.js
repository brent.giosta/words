import AssessmentService from "../services/core/assessment";
import WordListService from "./core/wordList";

class WordStatusService {
  async getWordStatusByStudent(studentId) {
    let assessments = await AssessmentService.getAllDescendingByStudent(studentId);

    let query = await WordListService.getAllAscending();

    return this.getWordStatusByAssessments(assessments, query);
  }

  async getAllWordStatuses() {
    let assessments = await AssessmentService.getAllDescending();

    let query = await WordListService.getAllAscending();

    let studentAssessments = new Map();
    assessments.forEach(assessment => {
      let assessmentList = studentAssessments.get(assessment.studentId);
      if (!assessmentList) {
        assessmentList = [];
        studentAssessments.set(assessment.studentId, assessmentList);
      }
      assessmentList.push(assessment);
    });

    let results = new Map();
    studentAssessments.forEach((value, key) => {
      results.set(key, this.getWordStatusByAssessments(value, query));
    });

    return results;
  }

  getWordStatusByAssessments(assessments, wordLists) {
    let wordListMap = new Map();

    /* ,
            lastResult: null,
            lastResultCount: 1,
            differentResult: false */
    /*          wordListRecord.record.unshift({ result: wordRecord.result, 
      timestamp: wordRecord.beginTime, 
      lastResultCount: wordListRecord.lastResultCount,
       differentResult: wordListRecord.differentResult });
 */
    assessments.forEach(assessment => {
      assessment.data.wordList.forEach(wordRecord => {
        if (wordRecord.result) {
          let wordListRecord = wordListMap.get(wordRecord.word) || {
            word: wordRecord.word,
            record: []
          };
          wordListMap.set(wordRecord.word, wordListRecord);
          wordListRecord.record.unshift({ result: wordRecord.result, timestamp: wordRecord.beginTime });
        }
      });
    });

    wordLists.forEach((wordListItem, level) => {
      var grade = level;
      wordListItem.data.wordList.forEach(word => {
        let wordListRecord = wordListMap.get(word);
        if (!wordListRecord){
          wordListRecord = {
            word: word,
            record: [],
            lastResult: null,
            lastResultCount: 0
          };
        } else {
          var lastResultCount = 0;
          var prevResult;
          var hasFails = false;
          wordListRecord.record.forEach(record=>{
            if ((prevResult || {}).result === record.result) {
              ++lastResultCount;
            } else if ((prevResult || {}).result === 'mastered' && record.result === 'correct') {
              ++lastResultCount;
            } else {
              lastResultCount = 0;
            }
            record.lastResultCount = lastResultCount + 1;

            if (record.result !== 'correct') {
              hasFails = true;
            }
            if (record.result === 'correct' && (!hasFails || (record.lastResultCount >=3))) {
              record.result = 'mastered';
            }
            prevResult = record;
          });
          if (prevResult) {
            wordListRecord.lastResult = prevResult.result;
            wordListRecord.lastResultCount = wordListRecord.lastResult.lastResultCount;
          }
        }
        wordListRecord.level = grade;
        wordListMap.set(word, wordListRecord);
      });
    });

    return wordListMap;
  }
}

const service = new WordStatusService();
export default service;
