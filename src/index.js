import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, withRouter } from "react-router-dom";
import "./index.css";
import AppRouter from "./AppRouter";
import registerServiceWorker from "./registerServiceWorker";

import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase";
import firebaseConfig from "./config/firebase";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";

import Typography from "material-ui/Typography";
import Drawer from "material-ui/Drawer";
import DrawerContent from "./components/drawer";
import MainAppBar from "./components/appBar";
import BottomNavigation from "./components/bottomNavigation";
import { Provider, connect } from "react-redux";
import store from "./redux/store";
import defaultWordLists from "./config/defaultWords";

firebase.initializeApp(firebaseConfig);

const SignInComponent = props => {
  return (
    <main style={{ marginTop: "56px", textAlign: "center" }}>
      <div style={{ display: "inline-block", width: 300 }}>
        <Typography variant="headline" gutterBottom>
          Sign In
        </Typography>
        <Typography variant="body2" gutterBottom>
          Please choose from the sign in options listed below:
        </Typography>
        <br />
      </div>
      <StyledFirebaseAuth uiConfig={props.uiConfig} firebaseAuth={firebase.auth()} />
    </main>
  );
};

class SignInScreen extends React.Component {
  // The component's Local state.
  state = {
    isSignedIn: false,
    drawerOpen: false
  };

  // Configure FirebaseUI.
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: "popup",
    // We will display Google and Facebook as auth providers.
    signInOptions: [firebase.auth.GoogleAuthProvider.PROVIDER_ID, firebase.auth.EmailAuthProvider.PROVIDER_ID],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccess: () => false
    }
  };

  async setupUserRecord(isSignedIn) {
    if (isSignedIn) {
      let currentUser = firebase.auth().currentUser;
      let userDocument = firebase
        .firestore()
        .collection("users")
        .doc(currentUser.uid);
      let user = await userDocument.get();
      if (!user.exists) {
        await userDocument.set({ id: currentUser.uid, name: currentUser.displayName });

        let wordsCollection = firebase
          .firestore()
          .collection("users")
          .doc(currentUser.uid)
          .collection("wordLists");

        for (let i = 0; i < defaultWordLists.length; ++i) {
          await wordsCollection.add({ timestamp: new Date().getTime(), wordList: defaultWordLists[i] });
        }
      }
    }
    this.setState({ isSignedIn: isSignedIn });
  }

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
      this.setupUserRecord(!!user);
    });
  }

  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  render() {
    let mainStyle = this.props.fullScreen
      ? { flex: "1 1 auto", WebkitFlex: "1 1 auto", position: "relative", height: "100%" }
      : {
          flex: "1 1 auto",
          WebkitFlex: "1 1 auto",
          position: "relative",
          overflowY: "auto",
          overflowX: "hidden",
          WebkitOverflowScrolling: "touch",
          marginTop: "16px",
          marginLeft: "8px",
          marginRight: "8px",
          marginBottom: "8px"
        };
    if (this.props.fullScreen && this.props.scrollable) {
      mainStyle.overflowY = "auto";
    }
    return (
      <div style={{ position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }}>
        <div style={{ height: "100%", display: "flex", flexDirection: "column" }}>
          <Drawer open={this.state.drawerOpen} onClose={() => this.setState({ drawerOpen: false })}>
            <DrawerContent onClose={() => this.setState({ drawerOpen: false })} />
          </Drawer>
          {this.props.fullScreen ? null : (
            <div style={{ flex: "0 0 auto", WebkitFlex: "0 0 auto" }}>
              <MainAppBar
                isSignedIn={this.state.isSignedIn}
                onToggleDrawer={() => {
                  this.setState(prevState => {
                    return { drawerOpen: !prevState.drawerOpen };
                  });
                }}
              />
            </div>
          )}
          <main style={mainStyle}>{this.state.isSignedIn ? <AppRouter /> : <SignInComponent uiConfig={this.uiConfig} />}</main>
          {this.props.fullScreen ? null : (
            <div style={{ flex: "0 0 auto", WebkitFlex: "0 0 auto" }}>
              <BottomNavigation />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
    fullScreen: store.appState.fullScreen,
    scrollable: store.appState.scrollable
  };
};

const App = withRouter(connect(mapStateToProps)(SignInScreen));

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);
registerServiceWorker();
