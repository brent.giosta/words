export default [
  [
    "no",
    "is",
    "can",
    "me",
    "you",
    "and",
    "he",
    "at",
    "a",
    "so",
    "on",
    "in",
    "up",
    "am",
    "we",
    "like",
    "see",
    "I",
    "go",
    "it",
    "do",
    "an",
    "the",
    "my",
    "to"
  ],
  [
    ..."than have over".split(" "),
    ..."about there ride".split(" "),
    ..."back any don’t".split(" "),
    ..."after into said".split(" "),
    ..."I’m just that".split(" "),
    ..."been little one".split(" "),
    ..."big make with".split(" "),
    ..."came before five".split(" "),
    ..."away two their".split(" "),
    ..."your four what".split(" "),
    ..."who mother but".split(" "),
    ..."when where here".split(" "),
    ..."them very going".split(" "),
    ..."because could our".split(" "),
    ..."from were three".split(" "),
    ..."want books".split(" "),
    ..."able good".split(" "),
    ..."bad help".split(" "),
    ..."give city".split(" "),
    ..."today write".split(" "),
    ..."week top".split(" "),
    ..."something room".split(" "),
    ..."bus under".split(" "),
    ..."year fast".split(" "),
    ..."can’t hill".split(" "),
    ..."tell know".split(" "),
    ..."across use".split(" "),
    ..."world let".split(" "),
    ..."cat place".split(" "),
    ..."take sleep".split(" "),
    ..."dad love".split(" "),
    ..."hide much".split(" "),
    ..."almost stay".split(" "),
    ..."dog name".split(" "),
    ..."anything new".split(" "),
    ..."home paper".split(" "),
    ..."down rain".split(" "),
    ..."become door".split(" "),
    ..."end fun".split(" "),
    ..."behind sky".split(" "),
    ..."fish both".split(" "),
    ..."why time".split(" "),
    "car"
  ],
  [
    ..."sea happy house".split(" "),
    ..."wrote catch start".split(" "),
    ..."again third grew".split(" "),
    ..."carry night way".split(" "),
    ..."wait goes friend".split(" "),
    ..."each last story".split(" "),
    ..."feel school street".split(" "),
    ..."always walk above".split(" "),
    ..."first ten find".split(" "),
    ..."ask change between".split(" "),
    ..."food outside every".split(" "),
    ..."work part should".split(" "),
    ..."brother live father".split(" "),
    ..."through party watch".split(" "),
    ..."funny game children".split(" "),
    ..."gave try hid".split(" "),
    ..."things pick enough".split(" "),
    ..."close right dark".split(" "),
    ..."even teach great".split(" "),
    ..."grow until inside".split(" "),
    ..."gone second light".split(" "),
    ..."same deep seen".split(" "),
    ..."knew view during".split(" "),
    ..."begin grade worn".split(" "),
    ..."winter snow wrong".split(" "),
    ..."must does you’re".split(" "),
    ..."stop together".split(" "),
    ..."several river".split(" "),
    ..."never might".split(" "),
    ..."getting air".split(" "),
    ..."earth I’d".split(" "),
    ..."group suddenly".split(" "),
    ..."baby easy".split(" "),
    ..."everything finally".split(" "),
    ..."high everyone".split(" "),
    ..."wouldn’t hold".split(" "),
    ..."probably special".split(" "),
    ..."through animal".split(" "),
    ..."against lost".split(" "),
    ..."hour beautiful".split(" "),
    ..."fight need".split(" "),
    ..."once job".split(" "),
    ..."best sick".split(" "),
    ..."ready maybe".split(" "),
    ..."free land".split(" "),
    ..."show next".split(" "),
    ..."build old".split(" "),
    ..."draw window".split(" "),
    ..."state better".split(" "),
    ..."kind written".split(" "),
    ..."circle favorite".split(" "),
    ..."large care".split(" "),
    ..."doing myself".split(" "),
    ..."family since".split(" "),
    ..."clothes picture".split(" "),
    ..."hand class".split(" "),
    ..."different idea".split(" "),
    ..."follow being".split(" "),
    ..."pretty also".split(" "),
    ..."couldn’t slowly".split(" "),
    ..."happen bring".split(" "),
    ..."themselves hear".split(" "),
    ..."direction often".split(" "),
    ..."nothing page".split(" "),
    ..."life store".split(" "),
    ..."someone while".split(" "),
    ..."without however".split(" "),
    ..."instead kids".split(" "),
    ..."either check".split(" "),
    ..."lunch listen".split(" "),
    ..."important few".split(" "),
    ..."less stuff".split(" "),
    ..."own problem".split(" "),
    ..."think such".split(" "),
    ..."round cleans".split(" "),
    ..."scared teacher".split(" "),
    ..."person dream".split(" "),
    ..."short sister".split(" "),
    ..."add plan".split(" "),
    ..."wanted they’re".split(" "),
    ..."young possible".split(" "),
    ..."question thought".split(" "),
    ..."yourself really".split(" "),
    ..."answer understand".split(" "),
    ..."money near".split(" "),
    ..."simple rest".split(" "),
    ..."more soon".split(" ")
  ]
];
