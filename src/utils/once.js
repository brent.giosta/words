/**
 * This class ensures an async function is executed only once at a time.  execute takes an async function and executes it, but won't allow it to execute again if an instance is already running.  
 * Use handleEvent to create an event handle for an async function which only needs to execute once.
 * 
 * Ex:
 * 
 * <Button onClick={Once.handleEvent(async () => {
 *  await doSomething();
 *  doSomethingElse();
 * })} />
 */

const ALREADY_EXECUTING = { message: "Once.execute was busy doing something else.  Try again later" };

export default class Once {
  executing = false;
  

  async execute(asyncFunction) {
    if (this.executing)
      return ALREADY_EXECUTING;
    
    this.executing = true;
    try {
      return await asyncFunction();
    } catch (e) {
      this.executing = false;
      throw e;
    }
  }

  handleEvent(asyncFunction) {
    return ()=>this.execute(asyncFunction);
  }
}