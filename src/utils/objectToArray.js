export default function objectToArray(o) {
  if (Array.isArray(o)) {
    return o;
  }
  let result = [];
  for (let i = 0; i < 99999999; ++i) {
    if (o[i] !== undefined) {
      result.push(o[i]);
    } else break;
  }
  return result;
}
