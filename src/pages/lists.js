import React, { PureComponent, Fragment } from "react";
import WordListService from "../services/core/wordList";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import Icon from "material-ui/Icon";
import List, { ListItem } from "material-ui/List";

import { Link } from "react-router-dom";
const fabStyle = { position: "fixed", right: 20, bottom: 56 + 20, zIndex: 20 };

class Page extends PureComponent {
  state = {
    list: []
  };

  async loadData() {
    try {
      let query = await WordListService.getAllAscending();
      this.setState({ data: query });
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }

  async addList() {
    await WordListService.post({ timestamp: new Date().getTime(), wordList: [] });
    this.loadData();
  }

  componentDidMount() {
    this.loadData();
  }

  renderData(data) {
    return (
      <Fragment>
        <Typography variant="headline" gutterBottom>
          Word Lists
        </Typography>

        <List>
          {data.map((datum, idx) => (
            <ListItem button key={datum.id} component={Link} to={"/lists/" + datum.id}>
              Level {idx === 0 ? "K" : idx}
            </ListItem>
          ))}
        </List>

        <Button variant="fab" color="secondary" style={fabStyle} onClick={() => this.addList()}>
          <Icon>add</Icon>
        </Button>
      </Fragment>
    );
  }

  render() {
    return this.state.data ? this.renderData(this.state.data) : null;
  }
}

export default Page;
