import React, { PureComponent, Fragment } from "react";
import firebase from "firebase";
import WordCloud from "../components/wordCloud";

class Page extends PureComponent {
  state = {
    wordCloud: null
  };

  async loadWordCloud() {
    try {
      let assessments = await firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("assessments")
        .orderBy("beginTimestamp", "desc")
        .get();

      let students = await firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("students")
        .get();

      let wordResults = new Map();

      students.docs.forEach(student => {
        let studentId = student.id;
        let studentWordState = new Map();
        let studentAssessments = assessments.docs.filter(assessment => assessment.data().studentId === studentId);

        studentAssessments.forEach(assessment =>
          assessment.data().wordList.forEach(wordItem => {
            let studentWordStateStatus = studentWordState.get(wordItem.word);
            let newStatus;
            switch (wordItem.result) {
              case "correct":
              case "incorrect":
              case "decoded":
                if (!studentWordStateStatus) newStatus = wordItem.result;
                break;
              default:
                break;
            }

            if (newStatus) {
              studentWordState.set(wordItem.word, newStatus);
            }
          })
        );

        studentWordState.forEach((value, key) => {
          let wordResult = wordResults.get(key) || { total: 0, correct: 0, incorrect: 0, decoded: 0 };
          wordResult.total++;
          wordResult[value]++;
          wordResults.set(key, wordResult);
        });
      });

      let wordCloud = [];

      wordResults.forEach((value, key) => {
        if (value.total > 0 && value.correct !== value.total) {
          //wordCloud[key] = (value.incorrect > 0 ? 1 : 0) + value.incorrect + value.decoded;
          wordCloud.push([key, (value.incorrect > 0 ? 1 : 0) + value.incorrect + value.decoded]);
        }
      });

      wordCloud = wordCloud.sort((a, b) => (a[1] > b[1] ? -1 : a[1] < b[1] ? 1 : 0));
      wordCloud = wordCloud.slice(0, 100);

      this.setState({ wordCloud: wordCloud });
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }

  componentDidMount() {
    this.loadWordCloud();
  }

  render() {
    return (
      <Fragment>
        {this.state.wordCloud ? (
          <WordCloud
            style={{ height: "100%", width: "100%", backgroundColor: "#C0C0C0", display: "flex" }}
            wordCloud={this.state.wordCloud}
          />
        ) : null}
      </Fragment>
    );
  }
}

export default Page;
