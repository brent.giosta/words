import React, { PureComponent } from "react";
import StudentService from "../services/core/student";
import Typography from "material-ui/Typography";
import Icon from "material-ui/Icon";
import WordStatusService from "../services/wordStatus";
import { storeManager } from "../redux/store";
import { Instant, LocalDateTime, LocalDate, DateTimeFormatter } from "js-joda";
import { Line } from "react-chartjs-2";
import Tabs, { Tab } from "material-ui/Tabs";

import { withRouter } from "react-router-dom";

const YYYY_MM_DD_format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

class Page extends PureComponent {
  state = {
    data: null,
    stacked: true
  };

  createChartData(wordList) {
    let chartData = {}; // key: date as string, value: { incorrect: num, decoded: num, correct: num, mastered: num }

    let dates = new Set();
    wordList.forEach(wordRecord => {
      wordRecord.record.forEach(wordResult => {
        dates.add(LocalDateTime.ofInstant(Instant.parse(wordResult.timestamp)).format(YYYY_MM_DD_format));
      });
    });
    dates.add(LocalDate.now().toString());
    if (dates.size === 1)
      dates.add(
        LocalDate.now()
          .plusDays(-1)
          .toString()
      );
    dates.forEach(date => {
      chartData[date] = { incorrect: 0, decoded: 0, correct: 0, mastered: 0 };
    });
    const dateArray = Array.from(dates).sort();
    wordList.forEach(wordRecord => {
      dateArray.forEach(date => {
        let localDate = LocalDate.parse(date);
        var status;
        var priorStatus;
        var priorStatusCount = 0;
        var neverStatus = true;
        wordRecord.record.forEach((wordResult, idx) => {
          let recordDate = LocalDate.ofInstant(Instant.parse(wordResult.timestamp));
          if (!recordDate.isAfter(localDate)) {
            status = wordResult.result;
            if (status === priorStatus) {
              priorStatusCount++;
            }
            if (status === "correct") {
              if (neverStatus) {
                status = "mastered";
              } else if (priorStatus === "mastered") {
                status = "mastered";
                priorStatusCount++;
              } else if (priorStatus === "correct" && priorStatusCount >= 2) {
                status = "mastered";
              }
            }
            if (status) {
              neverStatus = false;
            }
            priorStatus = status;
          }
        });
        if (status) chartData[date][status]++;
      });
    });
    const dataByStatus = status => dateArray.map(date => chartData[date][status]);
    let datasets = [
      { status: "incorrect", borderColor: "red", backgroundColor: "#FF9999", label: "Incorrect" },
      { status: "decoded", borderColor: "#FFCC00", backgroundColor: "#FFFF99", label: "Decoded" },
      { status: "correct", borderColor: "#0066FF", backgroundColor: "#99FFFF", label: "Correct" },
      { status: "mastered", borderColor: "green", backgroundColor: "#99FF99", label: "Mastered" }
    ].map(status => ({
      backgroundColor: status.backgroundColor,
      borderColor: status.borderColor,
      data: dataByStatus(status.status),
      label: status.label,
      pointRadius: 0,
      cubicInterpolationMode: "monotone"
    }));
    return { labels: dateArray, datasets: datasets };
  }

  async loadAssessments(studentId) {
    let student = await StudentService.get(studentId);

    let studentWordList = await WordStatusService.getWordStatusByStudent(studentId);

    let chartData = this.createChartData(studentWordList);
    this.setState({ data: { student: student, chart: chartData } });
    storeManager.setTitle("Report - " + student.studentName);
  }

  componentWillUnmount() {
    storeManager.setTitle();
  }

  componentDidMount() {
    this.loadAssessments(this.props.match.params.studentId);
  }

  renderReport(data) {
    const mapChartData = chart => ({
      labels: chart.labels,
      datasets: chart.datasets.map(dataset => ({ ...dataset, fill: this.state.stacked }))
    });
    let chartData = mapChartData(data.chart);
    return (
      <div
        style={{
          height: "100%",
          width: "100%",
          position: "absolute",
          display: "flex",
          flexDirection: "column",
          WebkitFlexDirection: "column"
        }}
      >
        <div style={{ width: "100%", flex: "0 0 auto", WebkitFlex: "0 0 auto" }}>
          <Typography variant="headline" gutterBottom>
            {data.student.studentName}
            <div style={{ float: "right" }}>
              <Tabs
                value={this.state.stacked ? 0 : 1}
                onChange={(event, value) => this.setState({ stacked: value === 0 ? true : false })}
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab style={{ minWidth: 64 }} icon={<Icon>landscape</Icon>} />
                <Tab style={{ minWidth: 64 }} icon={<Icon>show_chart</Icon>} />
              </Tabs>
            </div>
          </Typography>
        </div>
        <div
          style={{
            flex: "1 1 auto",
            WebkitFlex: "1 1 auto",
            position: "relative",
            overflow: "none",
            alignItems: "center",
            justifyContent: "center",
            display: "flex"
          }}
        >
          <Line
            data={chartData}
            options={{
              maintainAspectRatio: false,
              scales: {
                xAxes: [
                  {
                    type: "time",
                    time: {
                      unit: "day"
                    }
                  }
                ],
                yAxes: [
                  {
                    stacked: this.state.stacked
                  }
                ]
              },
              tooltips: {
                intersect: false,
                mode: "index",
                callbacks: {
                  label: function(tooltipItems, data) {
                    return data.datasets[tooltipItems.datasetIndex].label + " " + tooltipItems.yLabel;
                  }
                }
              }
            }}
          />
        </div>
      </div>
    );
  }

  render() {
    return this.state.data ? this.renderReport(this.state.data) : null;
  }
}

export default withRouter(Page);
