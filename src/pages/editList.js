import React, { PureComponent, Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import WordListService from "../services/core/wordList";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import Input, { InputLabel, InputAdornment } from "material-ui/Input";
import { FormControl } from "material-ui/Form";
import Icon from "material-ui/Icon";
import IconButton from "material-ui/IconButton";

const fabStyle = { position: "fixed", right: 20, bottom: 56 + 20, zIndex: 20 };
class WordListItem extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.data !== this.props.data;
  }

  render() {
    let props = this.props;
    return (
      <FormControl fullWidth margin="normal">
        <InputLabel htmlFor={"word" + props.index}>Word {props.index + 1}</InputLabel>
        <Input
          id={"word" + props.index}
          type={"text"}
          autoFocus={props.autoFocus}
          value={props.data}
          onChange={props.onChange}
          endAdornment={
            <InputAdornment position="end">
              <IconButton aria-label="Delete item" onClick={props.onDelete}>
                <Icon>delete</Icon>
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    );
  }
}
class Page extends PureComponent {
  state = {
    originalRecord: null,
    list: [],
    dirty: false
  };

  async loadData(listId) {
    try {
      let query = await WordListService.getAllAscending();
      let list, level, originalRecord;
      query.forEach((datum, idx) => {
        if (datum.id === listId) {
          originalRecord = datum.data;
          list = [...originalRecord.wordList];
          level = idx;
        }
      });
      this.setState({ list: list, level: level, originalRecord: originalRecord });
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }

  addItem() {
    let newList = [...this.state.list];
    let index = newList.push("") - 1;
    this.setState({ dirty: true, list: newList, focusIndex: index });
  }

  componentDidMount() {
    this.loadData(this.props.match.params.listId);
  }

  async validateAndSave() {
    let obj = { ...this.state.originalRecord };
    obj.wordList = this.state.list;
    await WordListService.put(this.props.match.params.listId, obj);
    this.props.history.goBack();
  }

  renderData(level) {
    return (
      <Fragment>
        <Typography variant="headline" gutterBottom>
          Level {level === 0 ? "K" : level}
        </Typography>
        {this.state.list.map((datum, idx) => (
          <WordListItem
            key={idx}
            autoFocus={idx === this.state.focusIndex}
            data={datum}
            index={idx}
            onDelete={event => {
              let newState = [...this.state.list];
              newState.splice(idx, 1);
              this.setState({
                list: newState,
                dirty: true,
                focusIndex: null
              });
            }}
            onChange={event => {
              let newState = [...this.state.list];
              newState[idx] = event.target.value;
              this.setState({
                list: newState,
                dirty: true,
                focusIndex: null
              });
            }}
          />
        ))}
        <Button variant="fab" style={fabStyle} color="secondary" onClick={this.addItem.bind(this)}>
          <Icon>add</Icon>
        </Button>
        <div style={{ marginTop: 16, marginBottom: 16 }}>
          <Button variant="raised" color="primary" onClick={() => this.validateAndSave()}>
            Save
          </Button>
        </div>
      </Fragment>
    );
  }

  render() {
    return this.state.originalRecord ? this.renderData(this.state.level) : null;
  }
}

export default withRouter(Page);
