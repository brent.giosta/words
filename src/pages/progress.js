import React, { PureComponent, Fragment } from "react";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import Radio, { RadioGroup } from "material-ui/Radio";
import { FormLabel, FormControl, FormControlLabel } from "material-ui/Form";
import { storeManager } from "../redux/store";

import { withRouter } from "react-router-dom";
import StudentService from '../services/core/student';
import {createProgressAssessment} from '../services/assessmentFactory';

class Page extends PureComponent {
  state = {
    wordCount: "30",
    generating: 0
  };

  async loadStudent(studentId) {
    let student = await StudentService.get(studentId);

    this.setState({
      studentName: student.studentName,
      loaded: true,
      student: { id: studentId, data: student }
    });

    storeManager.setTitle(student.studentName);
  }

  componentDidMount() {
    if (this.props.match.params.studentId) {
      this.loadStudent(this.props.match.params.studentId);
    }
  }

  componentWillUnmount() {
    storeManager.setTitle();
  }

  beginAssessment() {
    this.setState(prevState => {
      if (prevState.generating === 0) {
        this.createProgressAssessmentAndNavigate();
        return { generating: 1 };
      }
    });
  }

  async createProgressAssessmentAndNavigate() {
    const studentId = this.props.match.params.studentId;
    const wordCount = this.state.wordCount;
    const student = this.state.student.data;

    let assessmentId = await createProgressAssessment(studentId, student, wordCount);

    this.props.history.replace("/assessments/" + assessmentId);
  }

  render() {
    let props = this.props;

    return this.state.generating === 1 ? (
      <div />
    ) : (
      <Fragment>
        <Typography variant="headline" gutterBottom>
          Progress Monitoring
        </Typography>

        {props.match.params.studentId && !this.state.loaded ? null : (
          <Fragment>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">Number of Words</FormLabel>
              <RadioGroup
                aria-label="Number of Words"
                name="wordNumber"
                value={this.state.wordCount}
                onChange={event => this.setState({ wordCount: event.target.value })}
              >
                <FormControlLabel value={"12"} control={<Radio />} label="12" />
                <FormControlLabel value={"30"} control={<Radio />} label="30" />
                <FormControlLabel value={"60"} control={<Radio />} label="60" />
                <FormControlLabel value={"90"} control={<Radio />} label="90" />
              </RadioGroup>
            </FormControl>
            <div style={{ marginTop: 16 }}>
              <Button type="submit" variant="raised" color="primary" onClick={() => this.beginAssessment()}>
                Begin
              </Button>
            </div>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

export default withRouter(Page);
