import React, { PureComponent, Fragment } from "react";
import AssessmentService from "../services/core/assessment";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import List, { ListItem, ListItemText, ListItemSecondaryAction } from "material-ui/List";
import objectToArray from "../utils/objectToArray";
import Icon from "material-ui/Icon";
import IconButton from "material-ui/IconButton";

import { Instant, LocalDateTime, DateTimeFormatter } from "js-joda";

import { withRouter } from "react-router-dom";

function formatLocalDateTime(instant) {
  return instant.format(DateTimeFormatter.ofPattern("M/d/yyyy h:mm:ss")) + (instant.hour() < 12 ? " AM" : " PM");
}

class Page extends PureComponent {
  state = {
    saving: 0,
    wordList: null,
    originalRecord: null
  };

  async loadAssessment(assessmentId) {
    let assessmentDoc = await AssessmentService.get(assessmentId);
    let assessment = assessmentDoc;
    let wordList = objectToArray(JSON.parse(JSON.stringify(assessment.wordList)));
    this.setState({
      originalRecord: assessment,
      wordList: wordList
    });
  }

  componentDidMount() {
    this.loadAssessment(this.props.match.params.assessmentId);
  }
  async saveAssessment() {
    await AssessmentService.patch(this.props.match.params.assessmentId, { wordList: this.state.wordList });
    this.props.history.goBack();
  }

  save = () => {
    this.setState(prevState => {
      if (prevState.saving === 0) {
        this.saveAssessment();
        return { saving: 1 };
      }
    });
  };

  async deleteAssessment() {
    await AssessmentService.delete(this.props.match.params.assessmentId);
    this.props.history.goBack();
  }

  delete = () => {
    this.setState(prevState => {
      if (prevState.saving === 0) {
        if (window.confirm("Are you sure you want to delete this assessment?")) {
          this.deleteAssessment();
          return { saving: 1 };
        }
      }
    });
  };

  process(idx, result) {
    let newResults = [...this.state.wordList];
    newResults[idx].result = result;
    this.setState({ wordList: newResults });
  }

  render() {
    return !this.state.originalRecord || this.state.saving === 1 ? (
      <div />
    ) : (
      <Fragment>
        <Typography variant="headline" gutterBottom>
          {this.state.originalRecord
            ? formatLocalDateTime(LocalDateTime.ofInstant(Instant.parse(this.state.originalRecord.beginTimestamp)))
            : "Loading..."}
        </Typography>

        <List>
          {this.state.wordList.map((wordItem, idx) => (
            <ListItem key={idx}>
              <ListItemText primary={wordItem.word} />
              {wordItem.result ? (
                <ListItemSecondaryAction>
                  <IconButton onClick={() => this.process(idx, "incorrect")}>
                    <Icon style={{ backgroundColor: "black", color: "#FF0000", fontSize: 36 }}>
                      {wordItem.result === "incorrect" ? "check_circle" : "brightness_1"}
                    </Icon>
                  </IconButton>
                  <IconButton onClick={() => this.process(idx, "decoded")}>
                    <Icon style={{ backgroundColor: "black", color: "#FFFF00", fontSize: 36 }}>
                      {wordItem.result === "decoded" ? "check_circle" : "brightness_1"}
                    </Icon>
                  </IconButton>
                  <IconButton onClick={() => this.process(idx, "correct")}>
                    <Icon style={{ backgroundColor: "black", color: "#00FF00", fontSize: 36 }}>
                      {wordItem.result === "correct" ? "check_circle" : "brightness_1"}
                    </Icon>
                  </IconButton>
                </ListItemSecondaryAction>
              ) : null}
            </ListItem>
          ))}
        </List>

        <div style={{ marginTop: 16 }}>
          <Button type="submit" variant="raised" color="primary" onClick={() => this.save()}>
            Save
          </Button>

          <Button
            type="button"
            variant="raised"
            color="secondary"
            style={{ float: "right", marginRight: 16 }}
            onClick={() => this.delete()}
          >
            Delete
          </Button>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(Page);
