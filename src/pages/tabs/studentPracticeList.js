import React, { PureComponent, Fragment } from "react";
import Button from "material-ui/Button";
import List, { ListItem } from "material-ui/List";
import WordStatusService from "../../services/wordStatus";
import AssessmentService from "../../services/core/assessment";
import assignNewPracticeList from "../../services/practiceList";
import StudentService from '../../services/core/student';
import WordListService from "../../services/core/wordList";
import Once from '../../utils/once';
import ResultChips from '../../components/resultChips';

export default class StudentPracticeList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {  };
    this.once = new Once();
  }
  async loadWordList(studentId) {
    let student = await StudentService.get(studentId);
    
    let assessmentList = await AssessmentService.getAllDescendingByStudent(studentId);
    let query = await WordListService.getAllAscending();
    let studentWordList = WordStatusService.getWordStatusByAssessments(assessmentList, query);
    this.setState({practiceList: student.practiceList, wordList: studentWordList});
  }

  componentWillMount() {
    this.loadWordList(this.props.studentId);
  }

  async generatePracticeList() {
    let newPracticeList = await assignNewPracticeList(
      this.props.studentId,
      this.state.wordList
    );
    this.setState({ practiceList: newPracticeList });
  }

  render() {
    return !this.state.wordList ? null : <Fragment>
      {this.state.practiceList ? (
        <List>
          {this.state.practiceList.map((word, idx) => (
            <ListItem key={idx}>
              <span style={{ marginRight: 16 }}>{word}</span>
              <ResultChips records={(this.state.wordList.get(word) || { record: [] }).record} />
            </ListItem>
          ))}
        </List>
      ) : (
        <div>No Words Assigned</div>
      )}
      <div style={{ marginTop: 16 }}>
        <Button type="submit" variant="raised" color="primary" onClick={this.once.handleEvent(async () => this.generatePracticeList())}>
          Generate New List
        </Button>
      </div>
    </Fragment>;
  }
}