import React, { PureComponent, Fragment } from "react";
import { withRouter, Link } from "react-router-dom";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import List, { ListItem, ListItemText, ListItemSecondaryAction } from "material-ui/List";
import Grid from "material-ui/Grid";
import Icon from "material-ui/Icon";
import AssessmentService from "../../services/core/assessment";

import { createBaselineAssessment } from '../../services/assessmentFactory';

import { Instant, LocalDateTime, DateTimeFormatter } from "js-joda";
import objectToArray from "../../utils/objectToArray";
import Once from '../../utils/once';


function formatLocalDateTime(instant) {
  return instant.format(DateTimeFormatter.ofPattern("M/d/yyyy h:mm:ss")) + (instant.hour() < 12 ? " AM" : " PM");
}

class StudentAssessmentsImpl extends PureComponent { 
  state = {}
  once = new Once();

  async loadComponent(studentId) {
    let assessmentList = await AssessmentService.getAllDescendingByStudent(studentId);
    this.setState({assessments: assessmentList});
  }

  componentWillMount() {
    this.loadComponent(this.props.studentId);
  }

  render() {
    const {student, studentId} = this.props;

    return !this.state.assessments ? null : <Fragment>
      <Grid container spacing={8} style={{ marginTop: 8, marginBottom: 16 }}>
        <Grid item xs={12} sm={6} style={{ textAlign: "center" }}>
          <Button variant="raised" color="primary" onClick={this.once.handleEvent(async () => {
            let assessmentId = await createBaselineAssessment(studentId, student);
            this.props.history.push("/assessments/" + assessmentId);
          })}>
            <Icon style={{ marginRight: 8 }}>assessment</Icon> Baseline Assessment
          </Button>
        </Grid>
        <Grid item xs={12} sm={6} style={{ textAlign: "center" }}>
          <Button variant="raised" color="primary" component={Link} to={"/students/" + studentId + "/progress"}>
            <Icon style={{ marginRight: 8 }}>show_chart</Icon> Progress Monitoring
          </Button>
        </Grid>
      </Grid>
      <Typography variant="subheading" gutterBottom>
        Assessments History
      </Typography>
      <List>
        {this.state.assessments.map(assessment => (
          <ListItem key={assessment.id} button component={Link} to={"/assessmentDetail/" + assessment.id}>
            <ListItemText
              primary={formatLocalDateTime(LocalDateTime.ofInstant(Instant.parse(assessment.data.beginTimestamp)))}
              secondary={assessment.data.currentIndex + "/" + objectToArray(assessment.data.wordList).length + " completed"}
            />
            {assessment.data.currentIndex < objectToArray(assessment.data.wordList).length ? (
              <ListItemSecondaryAction>
                <Button component={Link} to={"/assessments/" + assessment.id}>
                  RESUME
                </Button>
              </ListItemSecondaryAction>
            ) : null}
          </ListItem>
        ))}
      </List>
    </Fragment>
  }
}

const StudentAssessments = withRouter(StudentAssessmentsImpl);
export default StudentAssessments;
