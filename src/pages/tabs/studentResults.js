import React, { PureComponent, Fragment } from "react";
import List, { ListItem, ListSubheader } from "material-ui/List";

import WordStatusService from '../../services/wordStatus';
import ResultChips from '../../components/resultChips';

export default class StudentResults extends PureComponent {
  state = {}

  componentWillMount() {
    this.loadResults(this.props.studentId);
  }

  async loadResults(studentId) {
    let wordResults = await WordStatusService.getWordStatusByStudent(studentId);
    let maxGradeLevel = Array.from(wordResults.values()).reduce((prevValue, currentValue, idx)=>{
      let result = currentValue.level > prevValue ? currentValue.level : prevValue
      return result;
    }, 0);
    let wordLists = [];
    for (let i = 0; i < maxGradeLevel+1; ++i) {
      wordLists.push([]);
    }
    wordResults.forEach((value, key)=>{
      if (value.level !== undefined) {
        wordLists[value.level].push({word: value.word, results: value.record});
      } else {
        console.error('word has no level: ' + key + ": " + JSON.stringify(value));
      }
    });

    this.setState({results: wordLists});
  }

  render() {
    return !this.state.results ? null : <Fragment>
      <List>
        {this.state.results.map((wordList, idx) => (
          <List
            key={idx}
            subheader={<ListSubheader style={{ backgroundColor: "white" }}>Level {idx === 0 ? "K" : idx}</ListSubheader>}
          >
            {wordList.map(word => (
              <ListItem key={word.word}>
                {word.word}
                <ResultChips records={word.results} />
              </ListItem>
            ))}
          </List>
        ))}
      </List>
    </Fragment>;
  }
}

