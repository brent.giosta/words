import React, { Fragment } from "react";
import Grid from "material-ui/Grid";
import Card, { CardHeader } from "material-ui/Card";
import Icon from "material-ui/Icon";
import Avatar from "material-ui/Avatar";
import ButtonBase from "material-ui/ButtonBase";
import { Link } from "react-router-dom";

const Page = () => {
  return (
    <Fragment>
      <Grid container spacing={24} style={{ marginTop: 48 }}>
        <Grid item xs={1} />
        <Grid item xs={10} sm={4}>
          <ButtonBase style={{ width: "100%" }} component={Link} to="/printList">
            <Card style={{ width: "100%" }}>
              <CardHeader
                avatar={
                  <Avatar>
                    <Icon>print</Icon>
                  </Avatar>
                }
                title="Print Practice Lists"
              />
            </Card>
          </ButtonBase>
        </Grid>
        <Grid item xs={1} sm={1} />
        <Grid item xs={1} sm={1} />
        <Grid item xs={10} sm={4}>
          <ButtonBase style={{ width: "100%" }} component={Link} to="/lists">
            <Card style={{ width: "100%" }}>
              <CardHeader
                avatar={
                  <Avatar>
                    <Icon>assignment</Icon>
                  </Avatar>
                }
                title="Word Lists"
              />
            </Card>
          </ButtonBase>
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={1} />
        <Grid item xs={10} sm={4}>
          <ButtonBase style={{ width: "100%" }} component={Link} to="/settings">
            <Card style={{ width: "100%" }}>
              <CardHeader
                avatar={
                  <Avatar>
                    <Icon>settings</Icon>
                  </Avatar>
                }
                title="Settings"
              />
            </Card>
          </ButtonBase>
        </Grid>
        <Grid item xs={1} sm={1} />
        <Grid item xs={1} sm={1} />
        <Grid item xs={10} sm={4}>
          <ButtonBase style={{ width: "100%" }} component={Link} to="/reports">
            <Card style={{ width: "100%" }}>
              <CardHeader
                avatar={
                  <Avatar>
                    <Icon>insert_chart</Icon>
                  </Avatar>
                }
                title="Reports"
              />
            </Card>
          </ButtonBase>
        </Grid>
        <Grid item xs={1} />
      </Grid>
    </Fragment>
  );
};

export default Page;
