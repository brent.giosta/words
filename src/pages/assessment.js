import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import Icon from "material-ui/Icon";
import IconButton from "material-ui/IconButton";
import { Instant } from "js-joda";
import { storeManager } from "../redux/store";
import withWidth from "material-ui/utils/withWidth";
import objectToArray from "../utils/objectToArray";
import { LinearProgress } from "material-ui/Progress";
import { CircularProgress } from "material-ui/Progress";
import Grid from "material-ui/Grid";
import Fireworks from "../components/fireworks";
import AssessmentService from "../services/core/assessment";

function fontByWidth(width) {
  switch (width) {
    case "xs":
      return 64;
    case "sm":
      return 84;
    case "md":
      return 128;
    default:
      return 160;
  }
}

class Page extends PureComponent {
  state = {
    word: "",
    wordList: {},
    status: "loading",
    contrast: false,
    currentIndex: 0,
    saving: 0,
    done: false
  };
  async loadAssessment(assessmentId) {
    let assessmentDoc = await AssessmentService.get(assessmentId);
    let assessment = assessmentDoc;
    let wordList = objectToArray(JSON.parse(JSON.stringify(assessment.wordList)));
    this.setState({
      originalRecord: assessment,
      wordList: wordList,
      currentIndex: assessment.currentIndex,
      status: "ready"
    });
  }
  async saveResults() {
    let assessment = { wordList: this.state.wordList, currentIndex: this.state.currentIndex };
    await AssessmentService.patch(this.props.match.params.assessmentId, assessment);
  }
  async saveResultsAndClose() {
    await this.saveResults();
    this.props.history.goBack();
  }
  pause() {
    this.setState(prevState => {
      if (prevState.saving === 0) this.saveResultsAndClose();
      return { saving: 1 };
    });
  }
  componentDidMount() {
    this.loadAssessment(this.props.match.params.assessmentId);
    storeManager.setFullScreen(true);
  }
  componentWillUnmount() {
    storeManager.setFullScreen(false);
  }
  process(result) {
    switch (this.state.status) {
      case "ready":
        this.setState({ word: this.state.wordList[this.state.currentIndex].word, status: "preDisplay" });
        setTimeout(() => {
          let newResults = [...this.state.wordList];
          newResults[this.state.currentIndex].beginTime = Instant.now().toString();
          this.setState({ wordList: newResults, status: "display" });
        }, 500);
        break;
      case "preDisplay":
        break;
      case "loading":
        break;
      case "display":
        let newResults = [...this.state.wordList];
        newResults[this.state.currentIndex].endTime = Instant.now().toString();
        newResults[this.state.currentIndex].result = result;
        let nextIndex = this.state.currentIndex + 1;
        let nextStatus = nextIndex >= this.state.wordList.length ? "done" : "preDisplay";
        this.setState({ wordList: newResults, status: nextStatus, currentIndex: nextIndex });
        if (nextStatus === "done") {
          this.saveResults();
          this.setState({ done: true });
          break;
        }
        setTimeout(() => {
          let newResults = [...this.state.wordList];
          newResults[this.state.currentIndex].beginTime = Instant.now().toString();
          this.setState({ wordList: newResults, status: "display", word: this.state.wordList[this.state.currentIndex].word });
        }, 500);
        break;
      default:
        break;
    }
  }

  renderAssessment() {
    return (
      <div style={{ height: "100%", width: "100%", display: "flex", flexDirection: "column", WebkitFlexDirection: "column" }}>
        <div style={{ width: "100%", backgroundColor: "#909090", flex: "0 0 auto", WebkitFlex: "0 0 auto" }}>
          <Grid container spacing={0}>
            <Grid item xs={2} md={1}>
              <IconButton onClick={() => this.pause()}>
                <Icon>close</Icon>
              </IconButton>
            </Grid>
            <Grid item xs={1} sm={2} md={3}>
              &nbsp;
            </Grid>
            <Grid item xs={6} sm={4} md={4}>
              {this.state.done ? null : (
                <table style={{ width: "100%" }}>
                  <tbody>
                    <tr>
                      <td style={{ textAlign: "center" }}>
                        <IconButton onClick={() => this.process("incorrect")}>
                          <Icon style={{ color: "#FF0000", fontSize: 36 }}>brightness_1</Icon>
                        </IconButton>
                      </td>
                      <td style={{ textAlign: "center" }}>
                        <IconButton onClick={() => this.process("decoded")}>
                          <Icon style={{ color: "#FFFF00", fontSize: 36 }}>brightness_1</Icon>
                        </IconButton>
                      </td>
                      <td style={{ textAlign: "center" }}>
                        <IconButton onClick={() => this.process("correct")}>
                          <Icon style={{ color: "#00FF00", fontSize: 36 }}>brightness_1</Icon>
                        </IconButton>
                      </td>
                    </tr>
                  </tbody>
                </table>
              )}
            </Grid>
            <Grid item xs={1} sm={2} md={3}>
              &nbsp;
            </Grid>
            <Grid item xs={2} md={1} style={{ textAlign: "right" }}>
              <IconButton onClick={() => this.setState({ contrast: !this.state.contrast })}>
                <Icon>iso</Icon>
              </IconButton>
            </Grid>
          </Grid>
        </div>
        {this.state.done ? (
          <Fireworks style={{ height: "100%", width: "100%", backgroundColor: "#000000" }} />
        ) : (
          <div
            style={{
              flex: "1 1 auto",
              WebkitFlex: "1 1 auto",
              position: "relative",
              overflow: "none",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              fontSize: fontByWidth(this.props.width),
              fontFamily: "Montserrat",
              backgroundColor: this.state.contrast ? "white" : "black",
              color: this.state.contrast ? "black" : "white",
              webkitTransform: "translateZ(0)"
            }}
          >
            <span
              style={{
                transition: this.state.status === "display" ? "none" : "opacity 0.25s linear",
                opacity: this.state.status === "display" ? 1 : 0
              }}
            >
              {this.state.word}
            </span>
          </div>
        )}
        <div style={{ width: "100%", flex: "0 0 auto", WebkitFlex: "0 0 auto" }}>
          <LinearProgress
            variant="determinate"
            style={{ height: 24 }}
            value={100 * (this.state.currentIndex / this.state.wordList.length)}
          />
        </div>
      </div>
    );
  }

  renderSpinner() {
    return (
      <div style={{ height: "100%", width: "100%", display: "flex", flexDirection: "column", WebkitFlexDirection: "column" }}>
        <div style={{ width: "100%", backgroundColor: "#909090", flex: "0 0 auto", WebkitFlex: "0 0 auto" }} />
        <div
          style={{
            flex: "1 1 auto",
            WebkitFlex: "1 1 auto",
            position: "relative",
            overflow: "none",
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            fontSize: fontByWidth(this.props.width),
            fontFamily: "Montserrat",
            backgroundColor: this.state.contrast ? "white" : "black",
            color: this.state.contrast ? "black" : "white"
          }}
        >
          <CircularProgress color="secondary" />
        </div>
        <div style={{ width: "100%", flex: "0 0 auto", WebkitFlex: "0 0 auto" }}>
          <LinearProgress
            variant="determinate"
            style={{ height: 24 }}
            value={100 * (this.state.currentIndex / this.state.wordList.length)}
          />
        </div>
      </div>
    );
  }

  render() {
    return this.state.saving === 0 ? this.renderAssessment() : this.renderSpinner();
  }
}

export default withRouter(withWidth()(Page));
