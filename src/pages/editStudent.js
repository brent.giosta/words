import React, { PureComponent, Fragment } from "react";
import StudentService from "../services/core/student";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import TextField from "material-ui/TextField";
import { MenuItem } from "material-ui/Menu";
import { FormControl, FormHelperText } from "material-ui/Form";
import { InputLabel } from "material-ui/Input";
import Select from "material-ui/Select";

import { withRouter } from "react-router-dom";

function blankIfNull(val) {
  return val === null || val === undefined ? "" : val;
}

class Page extends PureComponent {
  state = {
    studentName: "",
    studentName_e: null,
    beginGradeLevel: "",
    beginGradeLevel_e: null,
    grade: "",
    grade_e: null,
    saving: 0,
    loaded: false
  };

  async loadStudent(studentId) {
    let student = await StudentService.get(studentId);

    this.setState({
      studentName: student.studentName,
      beginGradeLevel: blankIfNull(student.beginGradeLevel),
      grade: blankIfNull(student.grade),
      loaded: true
    });
  }

  componentDidMount() {
    if (this.props.match.params.studentId) {
      this.loadStudent(this.props.match.params.studentId);
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
      [name + "_e"]: null
    });
  };

  async saveStudent() {
    if (this.props.match.params.studentId) {
      await StudentService.patch(this.props.match.params.studentId, {
        studentName: this.state.studentName,
        grade: this.state.grade,
        beginGradeLevel: this.state.beginGradeLevel
      });
    } else {
      await StudentService.post({
        studentName: this.state.studentName,
        grade: this.state.grade,
        beginGradeLevel: this.state.beginGradeLevel
      });
    }
    this.props.history.goBack();
  }

  save = () => {
    this.setState(prevState => {
      if (prevState.saving === 0) {
        this.saveStudent();
        return { saving: 1 };
      }
    });
  };

  validateAndSave = () => {
    let errors = false;

    if (this.state.studentName.length === 0) {
      errors = true;
      this.setState({ studentName_e: "Student Name is required" });
    } else {
      this.setState({ studentName_e: null });
    }

    if (this.state.grade === "") {
      errors = true;
      this.setState({ grade_e: "Current Grade Level is required" });
    } else {
      if (this.state.beginGradeLevel && parseInt(this.state.beginGradeLevel, 10) > parseInt(this.state.grade, 10)) {
        errors = true;
        this.setState({ grade_e: "Current Grade Level must be equal or greater the begin grade level" });
      } else {
        this.setState({ grade_e: null });
      }
    }

    if (!errors) {
      // save and navigate
      this.save();
    }
  };

  render() {
    let props = this.props;

    return this.state.saving === 1 ? (
      <div />
    ) : (
      <form
        onSubmit={event => {
          event.preventDefault();
          if (props.match.params.studentId && !this.state.loaded) return false;
          this.validateAndSave();
          return false;
        }}
      >
        <Typography variant="headline" gutterBottom>
          {props.match && props.match.params.studentId ? "Update Student Info" : "New Student"}
        </Typography>

        {props.match.params.studentId && !this.state.loaded ? null : (
          <Fragment>
            <TextField
              id="name"
              fullWidth
              label="Student Name"
              value={this.state.studentName}
              onChange={this.handleChange("studentName")}
              error={this.state.studentName_e ? true : false}
              autoFocus
              helperText={this.state.studentName_e}
              margin="normal"
            />

            <FormControl style={{ marginTop: 16 }} fullWidth error={this.state.grade_e ? true : false}>
              <InputLabel htmlFor="grade">Actual Grade Level</InputLabel>
              <Select
                value={this.state.grade}
                onChange={this.handleChange("grade")}
                inputProps={{
                  name: "grade",
                  id: "grade"
                }}
              >
                <MenuItem value={0}>K</MenuItem>
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4</MenuItem>
                <MenuItem value={5}>5</MenuItem>
              </Select>
              <FormHelperText>{this.state.grade_e}</FormHelperText>
            </FormControl>

            <FormControl style={{ marginTop: 16 }} fullWidth error={this.state.beginGradeLevel_e ? true : false}>
              <InputLabel htmlFor="beginGradeLevel">Beginning Grade Level</InputLabel>
              <Select
                value={this.state.beginGradeLevel}
                onChange={this.handleChange("beginGradeLevel")}
                inputProps={{
                  name: "beginGradeLevel",
                  id: "beginGradeLevel"
                }}
              >
                <MenuItem value={0}>K</MenuItem>
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4</MenuItem>
                <MenuItem value={5}>5</MenuItem>
              </Select>
              <FormHelperText>{this.state.beginGradeLevel_e}</FormHelperText>
            </FormControl>

            <div style={{ marginTop: 16 }}>
              <Button type="submit" variant="raised" color="primary">
                Save
              </Button>
            </div>
          </Fragment>
        )}
      </form>
    );
  }
}

export default withRouter(Page);
