import React, { PureComponent, Fragment } from "react";
import StudentService from "../services/core/student";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import Icon from "material-ui/Icon";
import List, { ListItem, ListItemText } from "material-ui/List";
import Avatar from "material-ui/Avatar";
import getInitials from "../utils/getInitials";
import { Link } from "react-router-dom";
const fabStyle = { position: "fixed", right: 20, bottom: 56 + 20, zIndex: 20 };

const StudentListItem = props => {
  let { student } = props;

  return (
    <ListItem button key={student.id} component={Link} to={"/students/" + student.id}>
      <Avatar>{getInitials(student.data.studentName)}</Avatar>
      <ListItemText primary={student.data.studentName} />
    </ListItem>
  );
};

class Page extends PureComponent {
  state = {
    students: []
  };

  async loadStudents() {
    try {
      let query = await StudentService.getAll();
      this.setState({
        students: query.sort((a, b) => {
          let aUpper = a.data.studentName.toUpperCase();
          let bUpper = b.data.studentName.toUpperCase();
          return aUpper > bUpper ? 1 : aUpper < bUpper ? -1 : 0;
        })
      });
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }

  componentDidMount() {
    this.loadStudents();
  }

  render() {
    return (
      <Fragment>
        <Typography variant="headline" gutterBottom>
          Students
        </Typography>

        <List>{this.state.students.map(student => <StudentListItem key={student.id} student={student} />)}</List>

        <Button variant="fab" color="secondary" style={fabStyle} component={Link} to="/students_add">
          <Icon>add</Icon>
        </Button>
      </Fragment>
    );
  }
}

export default Page;
