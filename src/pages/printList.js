import React, { PureComponent, Fragment } from "react";
import { withRouter } from "react-router-dom";
import StudentService from "../services/core/student";
import { storeManager } from "../redux/store";
import withWidth from "material-ui/utils/withWidth";
import Grid from "material-ui/Grid";
import Button from "material-ui/Button";
import Icon from "material-ui/Icon";

function rowsFromStudent(practiceList) {
  let result = [];
  for (let i = 0; i < 12; ++i) {
    if (i >= practiceList.length) {
      result.push("");
    } else {
      result.push(practiceList[i]);
    }
  }
  let dualResult = [];
  for (let i = 0; i < 6; ++i) {
    dualResult.push([result[i * 2], result[i * 2 + 1]]);
  }
  return dualResult;
}

class Page extends PureComponent {
  state = {};

  async loadStudents() {
    let students = (await StudentService.getAll())
      .map(doc => doc.data)
      .filter(student => student.practiceList && student.practiceList.length > 0)
      .sort((a, b) => {
        let aUpper = a.studentName.toUpperCase();
        let bUpper = b.studentName.toUpperCase();
        return aUpper > bUpper ? 1 : aUpper < bUpper ? -1 : 0;
      });

    this.setState({ students: students });
  }

  componentDidMount() {
    storeManager.setFullScreen(true, true);
    this.loadStudents();
  }
  componentWillUnmount() {
    storeManager.setFullScreen(false);
  }

  renderStudents(students) {
    const colStyle = {
      padding: "12pt",
      paddingTop: "24pt",
      paddingBottom: "24pt",
      fontSize: "28pt",
      textAlign: "center",
      border: "dashed 2pt black",
      width: "50%",
      margin: 0
    };
    let result = this.state.students.map((student, studentIdx) => {
      return (
        <div key={"student" + studentIdx} style={{ pageBreakInside: "avoid" }}>
          <div style={{ marginBottom: "12pt", marginTop: "12pt", marginLeft: "12pt", marginRight: "12pt", fontSize: "24pt" }}>
            {student.studentName}
          </div>
          <table
            style={{
              width: "calc(100% - 24pt)",
              marginLeft: "12pt",
              marginRight: "12pt",
              marginBottom: "12pt",
              borderCollapse: "collapse"
            }}
          >
            <tbody>
              {rowsFromStudent(student.practiceList).map((row, rowIdx) => (
                <tr key={"row" + rowIdx}>
                  {row.map((col, colIdx) => (
                    <td style={colStyle} key={"col" + colIdx}>
                      {col ? col : <span>&nbsp;</span>}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
    });
    return result;
  }

  render() {
    return (
      <Fragment>
        <div className="noPrint">
          <Grid container spacing={0}>
            <Grid item xs={6}>
              <Button onClick={() => this.props.history.goBack()}>
                <Icon>keyboard_arrow_left</Icon>&nbsp;Back
              </Button>
            </Grid>
            <Grid item xs={6} style={{ textAlign: "right" }}>
              <Button onClick={() => window.print()}>
                Print&nbsp;<Icon>print</Icon>
              </Button>
            </Grid>
          </Grid>
        </div>
        {this.state.students ? this.renderStudents(this.state.students) : null}
      </Fragment>
    );
  }
}

export default withRouter(withWidth()(Page));
