import React, { PureComponent } from "react";
import Fireworks from "../components/fireworks";

class Page extends PureComponent {
  render() {
    return <Fireworks style={{ height: "100%", width: "100%", backgroundColor: "#000000", display: "flex" }} />;
  }
}

export default Page;
