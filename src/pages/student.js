import React, { PureComponent, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import StudentService from "../services/core/student";
import StudentAssessments from './tabs/studentAssessments';
import { storeManager } from "../redux/store";
import StudentPracticeList from './tabs/studentPracticeList';
import StudentResults from './tabs/studentResults';

class Page extends PureComponent {
  state = {}

  async loadStudent(studentId) {
    let student = await StudentService.get(studentId);

    this.setState({
      student: { id: studentId, data: student } });
    storeManager.setTitle(student.studentName);
  }

  componentDidMount() {
    this.loadStudent(this.props.match.params.studentId);
    storeManager.setTabs([
      { title: "Assessments" },
      { title: "Practice List" },
      { title: "Results" },
      { title: "Report", link: "/students/" + this.props.match.params.studentId + "/report" }
    ]);
    storeManager.setActionButton("mode_edit", "/students_edit/" + this.props.match.params.studentId);
  }

  componentWillUnmount() {
    storeManager.setTitle(null);
    storeManager.setTabs(null);
    storeManager.setActionButton();
  }

  renderStudent(student) {
    return (
      <Fragment>
        {this.props.tabIndex === 0 && (
          <StudentAssessments studentId={this.state.student.id} student={this.state.student.data} />
        )}
        {this.props.tabIndex === 2 && (
          <StudentResults studentId={this.state.student.id} />
        )}
        {this.props.tabIndex === 1 && (
          <StudentPracticeList studentId={this.state.student.id} student={this.state.student.data} />
        )}
      </Fragment>
    );
  }

  render() {
    return <Fragment>{this.state.student ? this.renderStudent(this.state.student) : null}</Fragment>;
  }
}

const mapStateToProps = function(store) {
  return {
    tabIndex: store.appState.tabIndex
  };
};

export default withRouter(connect(mapStateToProps)(Page));
